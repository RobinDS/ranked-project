import { defaultsDeep } from 'lodash';

export class User {
  id: number;
  pseudo: string;
  email: string;
  // password: string;

  constructor(user?: Partial<User>) {
    defaultsDeep(this, user);
  }
}

import { defaultsDeep } from 'lodash';

export class Theme {
    id: number;
    userId: number;
    mainText: string;
    determinant1: string;
    basseIntensiteText: string;
    determinant2: string;
    hauteIntensiteText: string;


    constructor(theme?: Partial<Theme>) {
        defaultsDeep(this, theme);
    }
}

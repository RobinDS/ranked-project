import { defaultsDeep } from 'lodash';

export class Joueur {
    numero: number;
    pseudo: string;
    // password: string;

    constructor(joueur?: Partial<Joueur>) {
        defaultsDeep(this, joueur);
    }
}
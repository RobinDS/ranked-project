import { defaultsDeep } from 'lodash';

export class Question {
    id: number;
    phrase: string;

    constructor(user?: Partial<Question>) {
        defaultsDeep(this, user);
    }
}

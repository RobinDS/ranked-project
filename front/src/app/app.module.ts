import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {InscriptionComponent} from './pages/auth/inscription/inscription.component';
import {HttpClientModule} from '@angular/common/http';
import {AccueilComponent} from './pages/accueil/accueil.component';
import {FormsModule} from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import {AuthService} from './services/auth.service';
import {ConnexionComponent} from './pages/auth/connexion/connexion.component';
import {PartieComponent} from './pages/partie/partie.component';
import {AddUserComponent} from './pages/temporaire/add-user/add-user.component';
import {ListUsersComponent} from './pages/temporaire/list-users/list-users.component';
import { AccueilEspaceUtilisateurComponent } from './pages/espace-utilisateur/accueil-espace-utilisateur/accueil-espace-utilisateur.component';
import { MonDeckComponent } from './pages/espace-utilisateur/mon-deck/mon-deck.component';
import { SupprimerThemeComponent } from './pages/espace-utilisateur/mon-deck/supprimer-theme/supprimer-theme.component';
import { AjouterThemeComponent } from './pages/espace-utilisateur/mon-deck/ajouter-theme/ajouter-theme.component';
import {ReglesComponent} from './pages/regles/regles.component';
import { CarteComponent } from './pages/partie/carte/carte.component';

@NgModule({
    declarations: [
        AppComponent,
        InscriptionComponent,
        ConnexionComponent,
        AccueilComponent,
        PartieComponent,
        AddUserComponent,
        ListUsersComponent,
        AccueilEspaceUtilisateurComponent,
        MonDeckComponent,
        ReglesComponent,
        AjouterThemeComponent,
        SupprimerThemeComponent,
        AjouterThemeComponent,
        CarteComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule
    ],
    providers: [AuthService],
    bootstrap: [AppComponent]
})
export class AppModule {
}

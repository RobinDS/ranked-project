import {Injectable} from '@angular/core';
import {Theme} from '../models/theme.model';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {timeout} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ThemeService {

    private url: string;
    constructor(private http: HttpClient) {
        this.url = environment.url;
    }

    // utilisé uniquement pour afficher tous les thèmes dans les pages temporaire de dev
    getThemes(): Observable<Theme[]> {
        return this.http.get<Theme[]>(`${this.url}/themes`).pipe(timeout(10000))
    }

    // Fonction permettant de corriger l'erreur de formatage des thèmes par défaut (remplacer les â€™ en ')
    // Erreur du au niveau de l'insertion des données SQL
    // C'est un fix temporaire, il faudrait idéalement le corriger directement au niveau de la bdd
    correctDefaultThemeFormat(themes: Theme[]): Theme[] {
        return themes.map(theme => {
            if (theme.userId === 1) {
                theme.mainText=theme.mainText.replace('â€™', '\'')
                theme.mainText=theme.mainText.replace('Ãª', 'ê')
                theme.mainText=theme.mainText.replace('Ã©', 'é')
                theme.mainText=theme.mainText.replace('Ã©', 'é')
                theme.mainText=theme.mainText.replace('Ã', 'à')

                theme.hauteIntensiteText=theme.hauteIntensiteText.replace('â€™', '\'')
                theme.hauteIntensiteText=theme.hauteIntensiteText.replace('Ãª', 'ê')
                theme.hauteIntensiteText=theme.hauteIntensiteText.replace('Ã©', 'é')
                theme.hauteIntensiteText=theme.hauteIntensiteText.replace('Ã', 'à')

                theme.basseIntensiteText=theme.basseIntensiteText.replace('â€™', '\'')
                theme.basseIntensiteText=theme.basseIntensiteText.replace('Ãª', 'ê')
                theme.basseIntensiteText=theme.basseIntensiteText.replace('Ã©', 'é')
                theme.basseIntensiteText=theme.basseIntensiteText.replace('Ã', 'à')

                theme.determinant1=theme.determinant1.replace('Ã', 'à')
                theme.determinant2=theme.determinant2.replace('Ã', 'à')

            }
            return theme
        });
    }


    getDeckThemesByUserId(userId: number): Observable<Theme[]> {
        return this.http.get<Theme[]>(`${this.url}/deck/themes/user_id:${userId}`).pipe(timeout(10000));
    }
    getDefaultThemes(): Observable<Theme[]>{
        return this.http.get<Theme[]>(`${this.url}/themes/default_themes`).pipe(timeout(10000));
    }

    addTheme(theme: Theme): Observable<Theme> {
        return this.http.post<any>(`${this.url}/themes`, theme).pipe(timeout(10000));
    }

    deleteTheme(themeId: number, userId: number): Observable<any> {
        return this.http.delete(`${this.url}/themes/themeId:${themeId}&userId:${userId}`).pipe(timeout(10000));
    }
}

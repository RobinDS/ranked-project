import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {InscriptionComponent} from './pages/auth/inscription/inscription.component';
import {ConnexionComponent} from './pages/auth/connexion/connexion.component';
import {AccueilComponent} from './pages/accueil/accueil.component';
import {PartieComponent} from './pages/partie/partie.component';
import {AddUserComponent} from './pages/temporaire/add-user/add-user.component';
import {ListUsersComponent} from './pages/temporaire/list-users/list-users.component';
// tslint:disable-next-line:max-line-length
import {AccueilEspaceUtilisateurComponent} from './pages/espace-utilisateur/accueil-espace-utilisateur/accueil-espace-utilisateur.component';
import { MonDeckComponent } from './pages/espace-utilisateur/mon-deck/mon-deck.component';
import { SupprimerThemeComponent } from './pages/espace-utilisateur/mon-deck/supprimer-theme/supprimer-theme.component';
import { AjouterThemeComponent } from './pages/espace-utilisateur/mon-deck/ajouter-theme/ajouter-theme.component';
import {ReglesComponent} from './pages/regles/regles.component';


const routes: Routes = [
  {path: 'inscription', component: InscriptionComponent},
  { path: 'connexion', component: ConnexionComponent },
  { path: 'temp/add', component: AddUserComponent },
  { path: 'temp/display_delete', component: ListUsersComponent },
  { path: 'espace-utilisateur', component: AccueilEspaceUtilisateurComponent },
  { path: 'mon-deck', component: MonDeckComponent },
  { path: 'mon-deck/modifier', component: SupprimerThemeComponent },
  { path: 'mon-deck/ajouter', component: AjouterThemeComponent },
  { path: '', component: AccueilComponent },
  { path: 'jouer', component : PartieComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

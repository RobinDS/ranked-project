import {Component, OnInit} from '@angular/core';
import firebase from 'firebase/app';
import {AuthService} from '../../../services/auth.service';
import {Router} from '@angular/router';


@Component({
    selector: 'app-accueil-espace-utilisateur',
    templateUrl: './accueil-espace-utilisateur.component.html',
    styleUrls: ['./accueil-espace-utilisateur.component.scss']
})
export class AccueilEspaceUtilisateurComponent implements OnInit {
    constructor(private router: Router) {
    }

    ngOnInit(): void {
        // this.isSomeoneConnected()
    }

    // isSomeoneConnected() {
    //     if (firebase.auth().currentUser == null) {
    //         this.router.navigate(['/connexion'])
    //     }
    // }

    onSignOut() {
        // this.authService.signOutUser();
        this.router.navigate(['**']);
    }

}

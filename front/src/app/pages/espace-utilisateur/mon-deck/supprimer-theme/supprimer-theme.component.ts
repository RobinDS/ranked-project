import {Component, OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {ThemeService} from '../../../../services/theme.service';
// import firebase from 'firebase/app';
import {UserService} from '../../../../services/user.service';
import {User} from '../../../../models/user.model';
import {Theme} from '../../../../models/theme.model';

@Component({
    selector: 'app-supprimer-theme',
    templateUrl: './supprimer-theme.component.html',
    styleUrls: ['./supprimer-theme.component.scss']
})
export class SupprimerThemeComponent implements OnInit {
    user: User
    themes: Theme[]

    constructor(private formBuilder: FormBuilder, private userService: UserService,
                private themeService: ThemeService, private router: Router) {
    }

    ngOnInit(): void {
        // this.isSomeoneConnected()
        //     .then(() => {
        //         this.getUserAndThemesByEmail(firebase.auth().currentUser.email)
        //     })
        //     .catch(e => console.log(e))
        this.getUserAndThemesByEmail('robin.desemlyen@epfedu.fr')
    }

    private getUserAndThemesByEmail(email: string) {
        this.userService.getUserByEmail(email).subscribe(user => {
                this.user = user
                console.log(this.user)
                this.getUserThemes()
            }
        )
    }

    private getUserThemes() {
        this.themeService.getDeckThemesByUserId(this.user.id).subscribe(themes => {
            this.themes = this.themeService.correctDefaultThemeFormat(themes);
            console.log(this.themes)
        })
    }

    deleteTheme(themeId: number, userId: number) {
        if (this.themes.length <= 10) {
            alert('Il faut un minimum de 10 thèmes dans son deck pour lancer une partie de Ranked !')
        } else {
            this.themeService.deleteTheme(themeId, userId).subscribe(() => {
                this.themes = this.themes.filter(theme => theme.id !== themeId)
            });
        }
    }

    // async isSomeoneConnected() {
    //     if (firebase.auth().currentUser == null) {
    //         await this.router.navigate(['/connexion'])
    //     }
    // }
}

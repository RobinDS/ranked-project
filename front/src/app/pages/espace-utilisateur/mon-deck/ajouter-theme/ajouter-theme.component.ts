import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {defaultsDeep} from 'lodash';
import {User} from '../../../../models/user.model';
import {UserService} from '../../../../services/user.service';
import {ThemeService} from '../../../../services/theme.service';
import {Router} from '@angular/router';
import firebase from 'firebase/app';

@Component({
    selector: 'app-ajouter-theme',
    templateUrl: './ajouter-theme.component.html',
    styleUrls: ['./ajouter-theme.component.scss']
})
export class AjouterThemeComponent implements OnInit {
    addThemeForm: FormGroup
    user: User
    displaySuccessMessage = false;

    constructor(private formBuilder: FormBuilder, private userService: UserService,
                private themeService: ThemeService, private router: Router) {
    }

    ngOnInit(): void {
        // this.isSomeoneConnected()
        this.initForm()
        this.getUserByEmail('robin.desemlyen@epfedu.fr');
    }

    private getUserByEmail(email: string) {
        this.userService.getUserByEmail(email).subscribe(user => {
                this.user = user
                console.log(this.user)
            }
        )
    }

    // isSomeoneConnected() {
    //     if (firebase.auth().currentUser == null) {
    //         this.router.navigate(['/connexion'])
    //     }
    // }

    initForm() {
        this.addThemeForm = this.formBuilder.group({
            mainText: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{2,}/)]],
            basseIntensiteText: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{2,}/)]],
            hauteIntensiteText: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{2,}/)]],
            determinant1: ['DE', [Validators.required]],
            determinant2: ['À', [Validators.required]],
        });
    }

    onSubmit() {
        const theme = defaultsDeep({
            id: null,
            userId: this.user.id,
            mainText: this.addThemeForm.get('mainText').value.toUpperCase(),
            determinant1: this.addThemeForm.get('determinant1').value.toUpperCase(),
            basseIntensiteText: this.addThemeForm.get('basseIntensiteText').value.toUpperCase(),
            determinant2: this.addThemeForm.get('determinant2').value.toUpperCase(),
            hauteIntensiteText: this.addThemeForm.get('hauteIntensiteText').value.toUpperCase(),
        });
        this.themeService.addTheme(theme).subscribe(() => {
                this.displaySuccessMessage = true;
                setTimeout(() => this.router.navigate(['/mon-deck']), 600)
            }
        );
    }

}

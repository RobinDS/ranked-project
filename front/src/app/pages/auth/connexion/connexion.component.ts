import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {UserService} from '../../../services/user.service';
import {defaultsDeep} from 'lodash';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-connexion',
    templateUrl: './connexion.component.html',
    styleUrls: ['./connexion.component.scss']
})
export class ConnexionComponent implements OnInit {
    signInForm: FormGroup
    errorMessage: string

    constructor(private formBuilder: FormBuilder,
                private userService: UserService,
                // private authService: AuthService,
                private router: Router) {
    }

    ngOnInit() {
        this.initForm();
    }

    initForm() {
        this.signInForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]]
        });
    }

    onSubmit() {
        const email = this.signInForm.get('email').value;
        const password = this.signInForm.get('password').value;

        // this.userService.getUserByEmail(email)
        this.router.navigate(['/espace-utilisateur']);

        // this.authService.signInUser(email, password).then(
        //     () => {
        //         this.router.navigate(['/espace-utilisateur']);
        //     },
        //     (error) => {
        //         console.log(typeof error)
        //         console.log(error)
        //         if (error.message === 'The email address is badly formatted.') {
        //             this.errorMessage = `Erreur : Email n'est pas au bon format `;
        //         } else if (error.message === 'There is no user record corresponding to this identifier. The user may have been deleted.') {
        //             this.errorMessage = `Erreur : Email non reconnu`;
        //         } else if (error.message === 'The password is invalid or the user does not have a password.') {
        //             this.errorMessage = `Erreur : Mot de passe non reconnu`;
        //             // tslint:disable-next-line:max-line-length
        //         } else if (error.message === 'A network error (such as timeout, interrupted connection or unreachable host) has occurred.') {
        //             this.errorMessage = `Erreur : Problème de connexion`;
        //         } else this.errorMessage = error;
        //     }
        // );
    }
}


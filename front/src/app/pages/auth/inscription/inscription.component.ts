import { Component, OnInit } from '@angular/core';
import {UserService} from '../../../services/user.service';
import { defaultsDeep } from 'lodash';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss']
})
export class InscriptionComponent implements OnInit {
  signupForm: FormGroup;
  errorMessage: string;

  constructor(private formBuilder: FormBuilder,
              // private authService: AuthService,
              private userService: UserService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.signupForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]],
      pseudo: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{4,}/)]]
    });
  }

  onSubmit() {
    const email: string = this.signupForm.get('email').value;
    const password  = this.signupForm.get('password').value;
    const pseudo: string = this.signupForm.get('pseudo').value;
    console.log(email)
    console.log(password)
    console.log(pseudo)
    const user = defaultsDeep({
      id: null,
      pseudo,
      email,
    });
    this.userService.addUser(user).subscribe(u => console.log(u));
    this.router.navigate(['/espace-utilisateur']);
    // this.authService.createNewUser(email, password).then(
    //     () => {
    //       this.userService.addUser(user).subscribe(u => console.log(u));
    //       this.router.navigate(['/espace-utilisateur']);
    //     },
    //     (error) => {
    //       if (error.message === 'The email address is badly formatted.') {
    //         this.errorMessage = `Erreur : Email n'est pas au bon format `;
    //       }
    //       else if (error.message === 'The email address is already in use by another account.') {
    //         this.errorMessage = `Erreur : Email déjà utilisé par un autre utilisateur`;
    //       }
    //       else this.errorMessage = error;
    //     }
    // );
  }
}


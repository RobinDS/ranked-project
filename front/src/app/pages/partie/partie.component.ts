import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
// import firebase from 'firebase/app';
import {UserService} from '../../services/user.service';
import {ThemeService} from '../../services/theme.service';
import {User} from '../../models/user.model';

@Component({
    selector: 'app-partie.component',
    templateUrl: './partie.component.html',
    styleUrls: ['./partie.component.scss']
})
export class PartieComponent implements OnInit {

    joueurs = []
    tableauJoueurs
    choixThemes
    finDuJeu
    user: User
    themeChoisi = null
    repartitionNumero
    prestation
    choixOrdreJoueur
    score
    error
    // on initialise themes avec des thèmes de test afin de pouvoir tester des fonctionnalités sans back-end
    themes = [{
        id: 1,
        userId: 1,
        mainText: 'Lisez un SMS imaginaire envoye par votre conjoint(e), ',
        determinant1: 'de',
        basseIntensiteText: ' celui qu on ne voudrait jamais recevoir',
        determinant2: 'a',
        hauteIntensiteText: ' celui qui fait le plus plaisir.'
    },
        {
            id: 2,
            userId: 1,
            mainText: 'Ce matin, au boulot, vous avez la tete dans le cul. A la machine a cafe, vous racontez votre nuit. ',
            determinant1: 'de',
            basseIntensiteText: ' la nuit la plus pourrave',
            determinant2: 'a',
            hauteIntensiteText: ' la nuit de reve.'
        },
        {
            id: 3,
            userId: 1,
            mainText: 'En cherchant quelque chose dans la chambre de vos parents, vous trouvez un coffret et vous l ouvrez... Qu est-ce ? ',
            determinant1: 'de',
            basseIntensiteText: ' pas genant',
            determinant2: 'a',
            hauteIntensiteText: ' tres genant.'
        },
        {
            id: 4,
            userId: 1,
            mainText: 'test 4',
            determinant1: 'de',
            basseIntensiteText: ' pas genant',
            determinant2: 'a',
            hauteIntensiteText: ' tres genant.'
        }, {
            id: 5,
            userId: 1,
            mainText: 'test 5',
            determinant1: 'de',
            basseIntensiteText: ' pas genant',
            determinant2: 'a',
            hauteIntensiteText: ' tres genant.'
        },
        {
            id: 6,
            userId: 1,
            mainText: 'test 6',
            determinant1: 'de',
            basseIntensiteText: ' pas genant',
            determinant2: 'a',
            hauteIntensiteText: ' tres genant.'
        },
        {
            id: 7,
            userId: 1,
            mainText: 'test 7',
            determinant1: 'de',
            basseIntensiteText: ' pas genant',
            determinant2: 'a',
            hauteIntensiteText: ' tres genant.'
        },
        {
            id: 8,
            userId: 1,
            mainText: 'test 8',
            determinant1: 'de',
            basseIntensiteText: ' pas genant',
            determinant2: 'a',
            hauteIntensiteText: ' tres genant.'
        },
        {
            id: 9,
            userId: 1,
            mainText: 'test 9',
            determinant1: 'de',
            basseIntensiteText: ' pas genant',
            determinant2: 'a',
            hauteIntensiteText: ' tres genant.'
        },
        {
            id: 10,
            userId: 1,
            mainText: 'test 10',
            determinant1: 'de',
            basseIntensiteText: ' pas genant',
            determinant2: 'a',
            hauteIntensiteText: ' tres genant.'
        },
        {
            id: 11,
            userId: 1,
            mainText: 'test 11',
            determinant1: 'de',
            basseIntensiteText: ' pas genant',
            determinant2: 'a',
            hauteIntensiteText: ' tres genant.'
        }]
    presta
    i
    tableauNumerosPresent = new Set([]);
    tableauNumerosNonPresent = new Set([]);
    num
    pseudo
    numero
    themeDejaPropose = new Set()
    themePropose1
    themePropose2
    revelation
    numeros = []
    nombrePartie
    // css variables
    isFlipped = false
    cardWith = 145
    cardHeight = 200

    constructor(private userService: UserService,
                private themeService: ThemeService) {
    }

    ngOnInit() {
        this.init();
        this.getDefaultThemes();
        this.getUserAndThemesByEmail('robin.desemlyen@epfedu.fr')

        // if (this.isSomeoneConnected()) {
        //     this.getUserAndThemesByEmail(firebase.auth().currentUser.email)
        // }
        // else {
        // }
    }

    onSubmit(ngForm: NgForm) {
        // @ts-ignore
        const pseudoInput = document.getElementById('pseudo');
        // @ts-ignore
        if (pseudoInput.value !== '' && this.joueurs.length < 10) {
            this.joueurs.push({
                numero: 0,
                nombreBouffon: 0,
                nombreCouronne: 0,
                capitaine: false,
                pseudo: ngForm.form.value.pseudo,
            });
        } else if (this.joueurs.length >= 10) {
            alert('Il y a deja 10 joueurs')
        }
        // @ts-ignore
        pseudoInput.value = '';
    }

    Jouer() {
        if (this.joueurs.length < 4) {
            alert('Il faut minimum 4 joueurs pour lancer une partie!');
        } else {
            // @ts-ignore
            if (this.joueurs.keys < 3) {
                this.error = true
            } else {
                this.tableauJoueurs = false
                while (this.themeDejaPropose.size < this.nombrePartie + 2) {
                    this.themes.map(theme => {
                        if (theme.id === Math.floor(Math.random() * 10 + 1)) {
                            this.themeDejaPropose.add(theme.id)
                            if (this.themePropose1 === null) {
                                this.themePropose1 = theme
                            } else if (this.themePropose2 === null) {
                                this.themePropose2 = theme
                            }
                        }
                    })
                }
                this.choixThemes = true
                this.joueurs[this.nombrePartie].capitaine = true
                this.pseudo = this.joueurs[this.nombrePartie].pseudo
                this.i = 0
                this.numero = Math.floor(Math.random() * 10 + 1)
                this.joueurs[this.i].numero = this.numero
                this.tableauNumerosPresent.add(this.numero)
                console.log(this.themes)
            }
        }
    }

    distributionNumero() {
        if (this.themeChoisi === null) {
            alert('Choississez un thème')
        } else {
            this.cardWith = 145
            this.cardHeight = 200
            this.nombrePartie = this.nombrePartie + 1
            this.choixThemes = false
            this.repartitionNumero = true
            this.pseudo = this.joueurs[this.i].pseudo
        }
    }

    init() {
        this.themePropose1 = null
        this.themePropose2 = null
        this.nombrePartie = 0
        this.tableauJoueurs = true
        this.i = 0
    }

    Vu() {
        // Retourne la carte si elle n'est pas face verso
        if (this.isFlipped) {
            this.isFlipped = false
        }
        // Delay pour laisser le temps à la carte de se retourner avant de changer de numéro
        setTimeout(() => this.nextNumeroAndPlayer(), 400)
    }

    private nextNumeroAndPlayer() {
        if (this.joueurs[this.i + 1] < this.joueurs.keys) {
            this.i = this.i + 1
            this.pseudo = this.joueurs[this.i].pseudo
            while (this.tableauNumerosPresent.size !== this.i + 1) {
                this.numero = Math.floor(Math.random() * 10 + 1)
                this.tableauNumerosPresent.add(this.numero)
            }
            this.joueurs[this.i].numero = this.numero
            // tslint:disable-next-line:prefer-for-of
        } else {
            this.repartitionNumero = false
            this.isFlipped = true
            this.cardWith = 100
            this.cardHeight = 150
            this.i = 0
            // tslint:disable-next-line:prefer-for-of
            for (let j = 1; j < 11; j++) {
                // tslint:disable-next-line:prefer-for-of
                for (let h = 0; h < this.joueurs.length; h++) {
                    if (this.joueurs[h].numero !== j) {
                        this.i++
                    }
                }
                if (this.i === this.joueurs.length) {
                    this.tableauNumerosNonPresent.add(j)
                }
                this.i = 0
            }
            this.prestation = true
        }
    }

    choixTheme(value) {
        // tslint:disable-next-line:prefer-const
        let button1 = document.getElementById('1')
        // tslint:disable-next-line:prefer-const
        let button2 = document.getElementById('2')
        if (value === 1) {
            this.themeDejaPropose.delete(this.themePropose2.id)
            this.themeDejaPropose.add(this.themePropose1.id)
            this.themeChoisi = this.themePropose1
            button1.classList.add('selected')
            button2.classList.remove('selected')
        } else if (value === 2) {
            this.themeDejaPropose.delete(this.themePropose1.id)
            this.themeDejaPropose.add(this.themePropose2.id)
            this.themeChoisi = this.themePropose2
            button2.classList.add('selected')
            button1.classList.remove('selected')

        }
    }

    ordreJoueur() {
        this.prestation = false
        this.choixOrdreJoueur = true
    }

    resultatOrdreJoueurs() {
        this.numeros = []
        const choixOrdreJoueurs = new Set([])
        let nombreBouffon = 0
        let nombreCouronne = 0
        let selectElmt
        for (let j = 1; j < 11; j++){
            this.tableauNumerosNonPresent.delete(j)
        }
        // tslint:disable-next-line:prefer-for-of
        for (let j = 0; j < this.joueurs.length; j++) {
            this.tableauNumerosPresent.delete(this.joueurs[j].numero)
            selectElmt = document.getElementById(this.joueurs[j].pseudo)
            this.numero = selectElmt.value
            choixOrdreJoueurs.add(this.numero)
        }
        if (choixOrdreJoueurs.size < this.joueurs.length) {
            alert('Choississez des joueurs différents!')
        } else {
            // tslint:disable-next-line:prefer-for-of
            for (let j = 0; j < this.joueurs.length; j++) {
                selectElmt = document.getElementById(this.joueurs[j].pseudo)
                this.numero = selectElmt.value
                this.numeros.push(this.numero)
                // tslint:disable-next-line:prefer-for-of
            }
            for (let j = 1; j < this.numeros.length; j++) {
                if (this.numeros[j] < this.numeros[j - 1] && this.numeros[j] !== '10') {
                    nombreBouffon = nombreBouffon + 1
                } else if (this.numeros[j] > this.numeros[j - 1] || this.numeros[j] === '10') {
                    nombreCouronne = nombreCouronne + 1
                }
            }
            if (nombreCouronne === this.numeros.length - 1) {
                nombreCouronne = 1
            } else if (nombreBouffon > 0) {
                nombreCouronne = 0
            }
            this.joueurs[this.nombrePartie - 1].nombreBouffon = nombreBouffon + this.joueurs[this.nombrePartie - 1].nombreBouffon
            this.joueurs[this.nombrePartie - 1].nombreCouronne = nombreCouronne + this.joueurs[this.nombrePartie - 1].nombreCouronne
            this.cardWith = 100
            this.cardHeight = 150
            this.revelation = true
        }
    }

    finJeu() {
        this.revelation = false
        this.choixOrdreJoueur = false
        this.score = true
    }

    partieSuivante() {
        if (this.nombrePartie === this.joueurs.length) {
            this.score = false
            this.finDuJeu = true
        } else {
            this.score = false
            this.isFlipped = false
            this.themePropose1 = ''
            this.themePropose2 = ''
            const listThemesTaille = this.themeDejaPropose.size + 2
            while (this.themeDejaPropose.size < (listThemesTaille)) {
                this.themes.map(theme => {
                    if (theme.id === Math.floor(Math.random() * 10 + 1)) {
                        this.themeDejaPropose.add(theme.id)
                        if (this.themePropose1 === '' && this.themeDejaPropose.size === (listThemesTaille - 1)) {
                            this.themePropose1 = theme
                            // tslint:disable-next-line:max-line-length
                        } else if (this.themePropose2 === '' && this.themeDejaPropose.size === (listThemesTaille)) {
                            this.themePropose2 = theme
                        }
                    }
                })
            }
            this.joueurs[this.nombrePartie].capitaine = true
            this.pseudo = this.joueurs[this.nombrePartie].pseudo
            this.i = 0
            this.numero = Math.floor(Math.random() * 10 + 1)
            this.joueurs[this.i].numero = this.numero
            this.tableauNumerosPresent.add(this.numero)
            this.choixThemes = true
            this.themeChoisi = null
        }
    }
    //
    // isSomeoneConnected(): boolean {
    //     return firebase.auth().currentUser != null;
    // }

    private getUserAndThemesByEmail(email: string) {
        this.userService.getUserByEmail(email).subscribe(user => {
                this.user = user
                this.getUserThemes()
            }
        )
    }

    private getUserThemes() {
        this.themeService.getDeckThemesByUserId(this.user.id).subscribe(themes => {
            this.themes = this.themeService.correctDefaultThemeFormat(themes);
        })
    }

    getDefaultThemes(){
        this.themeService.getDefaultThemes().subscribe(themes => {
            this.themes = this.themeService.correctDefaultThemeFormat(themes);
        })
    }

}

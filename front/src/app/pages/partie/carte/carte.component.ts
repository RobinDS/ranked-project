import {Component, Input, OnInit,Output,EventEmitter} from '@angular/core';

@Component({
    selector: 'app-carte',
    templateUrl: './carte.component.html',
    styleUrls: ['./carte.component.scss']
})
export class CarteComponent implements OnInit {
    @Input() numero!: number;
    @Input() cardWith!: number;
    @Input() cardHeight!: number;
    @Input() isFlipped!: boolean;
    @Output() flipCardEvent = new EventEmitter<boolean>();

    constructor() {
    }

    ngOnInit(): void {
    }

    flipOnClick() {
        this.isFlipped = !this.isFlipped
        this.flipCardEvent.emit(this.isFlipped)
    }

}

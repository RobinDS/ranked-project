import {Component, OnInit} from '@angular/core';
import {User} from '../../../models/user.model';
import {UserService} from '../../../services/user.service';
import {Theme} from '../../../models/theme.model';
import {ThemeService} from '../../../services/theme.service';
import { defaultsDeep } from 'lodash';


// import firebase from 'firebase/auth';
import {NgForm} from '@angular/forms';


@Component({
    selector: 'app-list-users',
    templateUrl: './list-users.component.html',
    styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent implements OnInit {

    users: User[];
    themes: Theme[];

    constructor(private userService: UserService, private themeService: ThemeService) {
    }

    ngOnInit() {
    //     firebase.onAuthStateChanged(
    //         (user) => {
    //             this.isAuth = !!user;
    //         }
    //     );
    //
    //     this.themeService.getThemes().subscribe(themes => {
    //         this.themes = this.themeService.correctDefaultThemeFormat(themes);
    //     });
    //     this.userService.getUsers().subscribe(users => this.users = users);
    //
    }

    deleteTheme(themeId: number,userId: number) {
        this.themeService.deleteTheme(themeId,userId).subscribe(()=> {
            this.themes = this.themes.filter(theme => theme.id !== themeId)
        });
    }

    deleteUser(id: number) {
        this.userService.deleteUser(id).subscribe(() => {
            this.users = this.users.filter(user => user.id !== id)
        });
    }

    onSubmit(ngForm: NgForm) {
        console.log(ngForm);
        const user = defaultsDeep({
            id: null,
            pseudo: 'popo',
            email: 'popo',
        });

        this.userService.addUser(user).subscribe(() => {
            this.users.push(user);
        });
    }



}

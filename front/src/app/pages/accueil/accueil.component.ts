import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import Typewriter from 'typewriter-effect/dist/core';

@Component({
    selector: 'app-accueil.component',
    templateUrl: './accueil.component.html',
    styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit{
    ngOnInit() {
        const txtAnim = document.querySelector('h5');
        new Typewriter(txtAnim, {
            loop: true,
            deleteSpeed: 10,
        })
            .changeDelay(10)
            .typeString('Imaginez un super-héros qui contrôle les objets connectés. Trouvez-lui un nom du <span class="extreme-low">plus badass</span> au <span class="extreme-high">plus claqué</span>')
            .pauseFor(3000)
            .deleteChars(110)
            .typeString('Vous êtes en speed dating. Dites une phrase pour séduire votre interlocuteur de <span class="extreme-low">la pire</span> à <span class="extreme-high">la meilleure</span>')
            .pauseFor(3000)
            .deleteChars(102)
            .typeString('Vos vacances de rêves tournent au cauchemar. Décrivez la raison de <span class="extreme-low">la plus sensée</span> à <span class="extreme-high">la plus improbable</span>')
            .pauseFor(3000)
            .start()
    }

    onSubmit(ngForm: NgForm) {
        console.log(ngForm);
    }
}

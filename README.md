<h2>Nom du projet</h2>

Bonjour 👋, bienvenue sur notre projet RANKED.

<h2>Description</h2>

Il s'agit d'un jeu coopératif qui se joue de 4 à 10 personnes. A chaque manche, un membre est élu en tant que Capitaine et choisi un thème. Puis, des numéros (de 1 à 10) sont générés et associés à chaque joueur qui devront répondre au thème. L'intensité de leur prestation dépend justement de ce numéro. Lorsque tout le monde a répondu, le Capitaine doit deviner l'ordre des numéros pour chaque joueur.

Afin de customiser ses parties, l'utilisateur a la possibilité de se créer un compte et de pouvoir ajouter ou supprimer des thèmes ! Ainsi, il peut se connecter pour chaque session Ranked et gérer son deck de thèmes.

Pour réaliser cette application, nous avons utilisé Angular pour le front-end afin d'exploiter principalement sa simplicité à créer une Single Page Application. En effet, ses services de routing, sa flexibilité et sa structure efficace d'échanges entre les composants réunissait tous les avantages que nous cherchions pour le jeu Ranked.
Le back-end est lui développer en Java sous SpringBoot avec une mariadb.

Etant un groupe de 3 créateurs, l'utilisation de git et s'exercer à ses bonnes pratiques en équipe a été au coeur du projet.

<h2>Comment installer le projet</h2>

<h3>Lancer la db :</h3>

Aller à la racine du dossier api/ et lancer la commande suivante :

Sur Windows :
```
docker run --name mariadb --rm -e MYSQL_ROOT_PASSWORD=toor -e MYSQL_DATABASE=defaultdb -p 3306:3306 -v "%cd%/initdb:/docker-entrypoint-initdb.d" mariadb:10.5.12
```

Sur Mac / Linux :
```
docker run --name mariadb --rm -e MYSQL_ROOT_PASSWORD=toor -e MYSQL_DATABASE=defaultdb -p 3306:3306 -v "$(pwd)/initdb:/docker-entrypoint-initdb.d" mariadb:10.5.12
```

Cela va lancer un container mariadb dans lequel va tourner la db du projet (appelée "defaultdb"). Il est important de se placer à la racine du dossier api/ afin que le container trouve le chemin vers ./initdb/ qui contient les fichiers sql qui vont paramétrer et initialiser la db du projet. 

<h3>Lancer le back-end :</h3>

Aller à la racine du dossier api/ et lancer la commande suivante :

```
mvn package && java -jar target/demo-0.0.1-SNAPSHOT.jar
```

le back-end est connecté au port 8080

versions utilisés :
mvn 3.6.3
java version "1.8.0_281"

<h3>Lancer le front-end :</h3>

Aller à la racine du dossier front/ et lancer la commande suivante :
```
ng serve
```
le front-end est connecté au port 4200.

Pour acceder à l'app -> http://localhost:4200/

versions utilisés :
Angular CLI: 12.0.2
Node: 14.17.0
Package Manager: npm 7.11.2
OS: win32 x64
con

<h3> Docker-compose </h3>

Une alternative de lancement par docker-compose viendra dans les jours à venir

<h2>Sécurité</h2>
<h3>FireBase : </h3>
Nous avons utilisé l'outil de Google FireBase pour le système de connection/authentification. Il nous permet d'héberger en temps réel les
emails et mot de passes (hashés) de nos utilisateurs.

A travers l'utilisation de FireBase, le projet prend en charge plusieurs erreurs :
<h4> A la l'inscription :</h4>
- format d'email<br/>
- email unique<br/>
- problème de connexion (timeout)<br/>

<h4> A la la connexion :</h4>
- format d'email<br/>
- email non-connu<br/>
- mot de passe non-reconnu<br/>
- problème de connexion (timeout)<br/>

<h2>Auteurs</h2> 

Nous sommes Marie, Robin et Antoine, 3 alternants en dernière année de l'EPF, passionnés de développement ! <br>Voici nos liens gitlab :
- https://gitlab.com/MarieDeville
- https://gitlab.com/RobinDS
- https://gitlab.com/akdy94

<h3>Remerciements</h3>
Remerciements à Alexandre Moray et Hugo Caupert pour nous avoir fourni un squelette de webapp angular/springboot ainsi que des conseils avisés tout au long du projet.

<h2>License</h2>

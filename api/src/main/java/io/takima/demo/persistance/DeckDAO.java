package io.takima.demo.persistance;
import io.takima.demo.models.Deck;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 */
@Repository
public interface DeckDAO extends CrudRepository<Deck, Long> {

}

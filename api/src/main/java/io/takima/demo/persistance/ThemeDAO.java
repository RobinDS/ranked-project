package io.takima.demo.persistance;

import io.takima.demo.models.Theme;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 */
@Repository
public interface ThemeDAO extends CrudRepository<Theme, Long> {

}

package io.takima.demo.models

import javax.persistence.*

/**
 *
 */
@Entity(name = "theme")
data class Theme(
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Id var id: Long?,
        @Column(name = "user_id") var userId: Long?,
        @Column(name = "main_text") var mainText: String?,
        @Column(name = "determinant1") var determinant1: String?,
        @Column(name = "basse_intensite_text") var basseIntensiteText: String?,
        @Column(name = "determinant2") var determinant2: String?,
        @Column(name = "haute_intensite_text") var hauteIntensiteText: String?) {
    constructor() : this(null, null, null, null, null,null,null)

}

package io.takima.demo.models

import javax.persistence.*

/**
 *
 */
@Entity(name = "deck")
data class Deck(
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id var id: Long?,
    @Column(name = "user_id") var userId: Long?,
    @Column(name = "theme_id") var themeId: Long?
) { constructor( userId: Long, themeId: Long) : this(null, userId, themeId)
}

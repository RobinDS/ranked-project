package io.takima.demo.models

import javax.persistence.*

/**
 *
 */
@Entity(name = "user")
data class User(
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Id var id: Long?,
        @Column(name = "pseudo") var pseudo: String?,
        @Column(name = "email") var email: String?
) {
    constructor() : this(null, null, null)


}

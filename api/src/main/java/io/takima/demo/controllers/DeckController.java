package io.takima.demo.controllers;

import io.takima.demo.models.Theme;
import io.takima.demo.persistance.DeckDAO;
import io.takima.demo.models.Deck;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping("/deck")
@CrossOrigin
public class DeckController {

    private final DeckDAO deckDAO;
    private final ThemeController themeController;

    public DeckController(DeckDAO deckDAO, ThemeController themeController) {
        this.deckDAO = deckDAO;
        this.themeController = themeController;
    }

    @GetMapping()
    public List<Deck> getDecks() {
        Iterable<Deck> it = this.deckDAO.findAll();
        List<Deck> decks = new ArrayList<>();
        it.forEach(decks::add);
        return decks;
    }


    @GetMapping("/id:{id}")
    public Deck findById(@PathVariable("id") Long id) {
        if (this.deckDAO.findById(id).isPresent()) {
            return this.deckDAO.findById(id).get();
        } else return new Deck();
    }

    @GetMapping("/themeId:{themeId}&userId:{userId}")
    public Deck findByUserIdAndThemeId(@PathVariable Long themeId, @PathVariable("userId") Long userId) {
        AtomicReference<Deck> deck = new AtomicReference<>(new Deck());
        Iterable<Deck> it = this.deckDAO.findAll();
        it.forEach(elt -> {
            if (Objects.equals(elt.getUserId(), userId) && Objects.equals(elt.getThemeId(), themeId)) {
                deck.set(elt);
            }
        });
        return deck.get();
    }


    @GetMapping("/themes/user_id:{user_id}")
    public List<Theme> getDeckThemesByUserId(@PathVariable("user_id") Long user_id) {
        List<Theme> userDeckThemes = new ArrayList<>();
        List<Deck> userDeck = getDeckByUserId(user_id);
        userDeck.forEach(elt -> userDeckThemes.add(themeController.findById(elt.getThemeId())));
        return userDeckThemes;
    }

    @GetMapping("/user_id:{user_id}")
    public List<Deck> getDeckByUserId(@PathVariable("user_id") Long user_id) {
        List<Deck> decks = new ArrayList<>();
        Iterable<Deck> it = this.deckDAO.findAll();
        it.forEach(t -> {
            if (Objects.requireNonNull(t.getUserId()).longValue() == user_id.longValue()) {
                decks.add(t);
            }
        });
        return decks;
    }


    @PostMapping()
    public Deck addTheme(@RequestBody Deck deck) {
        return this.deckDAO.save(deck);
    }

    @DeleteMapping("/{id}")
    public void deleteDeck(@PathVariable Long id) {
        this.deckDAO.deleteById(id);
    }

}

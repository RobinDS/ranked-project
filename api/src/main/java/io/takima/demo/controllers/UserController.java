package io.takima.demo.controllers;

import io.takima.demo.models.Deck;
import io.takima.demo.models.Theme;
import io.takima.demo.persistance.UserDAO;
import io.takima.demo.models.User;
import org.mariadb.jdbc.internal.logging.Logger;
import org.mariadb.jdbc.internal.logging.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping("/users")
@CrossOrigin
public class UserController {

    private final UserDAO userDAO;
    private final ThemeController themeController;
    private final DeckController deckController;


    public UserController(UserDAO userDAO, ThemeController themeController, DeckController deckController) {
        this.userDAO = userDAO;
        this.themeController = themeController;
        this.deckController = deckController;
    }

    @GetMapping()
    public List<User> getUsers() {
        Iterable<User> it = this.userDAO.findAll();
        List<User> users = new ArrayList<>();
        it.forEach(users::add);
        return users;
    }

    @GetMapping("/{email}")
    public User getUserByEmail(@PathVariable("email") String email) {
        AtomicReference<User> user = new AtomicReference<>(new User());
        Iterable<User> it = this.userDAO.findAll();
        it.forEach(u -> {
            if (Objects.equals(u.getEmail(), email)) {
                user.set(u);
            }
        });
        return user.get();//renvoie un user initialisé à null s'il ne trouve pas de user avec l'email correspondant
    }

    @PostMapping()
    public void addUser(@RequestBody User user) {
        // user.getId() renvoie null car il est nul !! Il prendra une valeur qu'apres avoir été save
        this.userDAO.save(user);
        user.setId(this.getUserByEmail(user.getEmail()).getId());

        Iterable<Theme> defaultThemes = themeController.getDefaultTheme();
        defaultThemes.forEach(theme -> {
            Deck deck = new Deck(user.getId(), theme.getId());
            deckController.addTheme(deck);
        });
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id) {
        Iterable<Deck> UserDeck = deckController.getDeckByUserId(id);
        UserDeck.forEach(elt -> {
            //On verifie que le thème à supprimer a n'est pas un thème par défaut
            if (Objects.equals(themeController.findById(elt.getThemeId()).getUserId(), id)) {
                deckController.deleteDeck(elt.getId());
                themeController.deleteTheme(elt.getThemeId(), id);
            } else {
                deckController.deleteDeck(elt.getId());
            }
        });
        this.userDAO.deleteById(id);
    }

}

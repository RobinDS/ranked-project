package io.takima.demo.controllers;

import io.takima.demo.models.Deck;
import io.takima.demo.persistance.DeckDAO;
import io.takima.demo.persistance.ThemeDAO;
import io.takima.demo.models.Theme;
import org.slf4j.ILoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping("/themes")
@CrossOrigin
public class ThemeController {

    private final ThemeDAO themeDAO;
    private final DeckDAO deckDAO;
    private static final Long DEFAULT_USER_ID = 1L;


    public ThemeController(ThemeDAO themeDAO, DeckDAO deckDAO) {
        this.themeDAO = themeDAO;
        this.deckDAO = deckDAO;
    }

    @GetMapping()
    public List<Theme> getThemes() {
        Iterable<Theme> it = this.themeDAO.findAll();
        List<Theme> themes = new ArrayList<>();
        it.forEach(themes::add);
        return themes;
    }

    @GetMapping("/id:{id}")
    public Theme findById(@PathVariable("id") Long id) {
        if (this.themeDAO.findById(id).isPresent()) {
            return this.themeDAO.findById(id).get();
        } else return new Theme();
    }

    @GetMapping("/user_id:{user_id}")
    public List<Theme> getThemeByUserId(@PathVariable("user_id") Long user_id) {
        List<Theme> themes = new ArrayList<>();
        Iterable<Theme> it = this.themeDAO.findAll();
        it.forEach(t -> {
            if (Objects.requireNonNull(t.getUserId()).longValue() == user_id.longValue()) {
                themes.add(t);
            }
        });
        return themes;
    }

    //Called to add all default themes to the new deck user's when created
    @GetMapping("/default_themes")
    public List<Theme> getDefaultTheme() {
        List<Theme> themes = new ArrayList<>();
        Iterable<Theme> it = this.themeDAO.findAll();
        Long defaultUserID = 1L;
        it.forEach(t -> {
            if (Objects.equals(t.getUserId(), defaultUserID)) {
                themes.add(t);
            }
        });
        return themes;
    }

    @PostMapping()
    public Theme addTheme(@RequestBody Theme theme) {
        this.themeDAO.save(theme);
        Deck deck = new Deck(theme.getUserId(), theme.getId());
        this.deckDAO.save(deck);
        return theme;
    }

    @DeleteMapping("/themeId:{themeId}&userId:{userId}")
    public void deleteTheme(@PathVariable Long themeId, @PathVariable Long userId) {
        deleteCorrespondingDeck(themeId,userId);
        //On s'assure de ne pas supprimer un thème par défaut
        Optional<Theme> optionalThemeToDelete = themeDAO.findById(themeId);
        optionalThemeToDelete.map(theme -> {
            if(!Objects.equals(theme.getUserId(), DEFAULT_USER_ID)){
                this.themeDAO.deleteById(themeId);
            }
            return theme;
        });
    }

    public void deleteCorrespondingDeck(Long themeId,Long userId){
        Iterable<Deck> it = this.deckDAO.findAll();
        it.forEach(elt -> {
            if (Objects.equals(elt.getUserId(), userId) && Objects.equals(elt.getThemeId(), themeId)) {
                this.deckDAO.delete(elt);
            }
        });
    }
}

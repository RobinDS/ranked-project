INSERT INTO rankeddb.user (id, pseudo, email) VALUES (1, 'default-acc', 'default@test.fr');
INSERT INTO rankeddb.user (id, pseudo, email) VALUES (2, 'Marie', 'marie.deville@epfedu.fr');
INSERT INTO rankeddb.user (id, pseudo, email) VALUES (3, 'Antoine', 'akeraudy@epfedu.fr');
INSERT INTO rankeddb.user (id, pseudo, email) VALUES (4, 'Robin', 'robin.desemlyen@epfedu.fr');

-- On rentre les thèmes "par défaut" : les thèmes par défauts seront commun à tous les utilisateurs.
INSERT INTO rankeddb.theme (id, user_id, main_text, determinant1, basse_intensite_text,determinant2, haute_intensite_text) VALUES (1, 1, 'Lisez un SMS imaginaire envoyé par votre conjoint(e),','de','celui qu’on ne voudrait jamais recevoir', 'à','celui qui fait le plus plaisir.');
INSERT INTO rankeddb.theme (id, user_id, main_text, determinant1, basse_intensite_text,determinant2, haute_intensite_text) VALUES (2, 1, 'Ce matin, au boulot, vous avez la tête dans le cul. A la machine à café, vous racontez votre nuit.','de','la nuit la plus pourrave', 'à','la nuit de rêve.');
INSERT INTO rankeddb.theme (id, user_id, main_text, determinant1, basse_intensite_text,determinant2, haute_intensite_text) VALUES (3, 1, 'En cherchant quelque chose dans la chambre de vos parents, vous trouvez un coffret et vous l ouvrez... Qu’est-ce ?', 'de','pas gênant', 'à','tres gênant.');
INSERT INTO rankeddb.theme (id, user_id, main_text, determinant1, basse_intensite_text,determinant2, haute_intensite_text) VALUES (4, 1, 'Quel est l’objet à garder sous son lit en cas de cambriolage ?','Du',' moins', 'au','plus utile.');
INSERT INTO rankeddb.theme (id, user_id, main_text, determinant1, basse_intensite_text,determinant2, haute_intensite_text) VALUES (5, 1, 'Quel est le lieu idéal pour un rencard ?','Du',' moins glamour', 'au','plus galant.');
INSERT INTO rankeddb.theme (id, user_id, main_text, determinant1, basse_intensite_text,determinant2, haute_intensite_text) VALUES (6, 1, 'Vous êtes invité au festival de Cannes. Mimez votre entrée sur le tapis rouge', 'de','la plus timide', 'a','la plus confiante.');
INSERT INTO rankeddb.theme (id, user_id, main_text, determinant1, basse_intensite_text,determinant2, haute_intensite_text) VALUES (7, 1, 'L’Etat annonce un nouveau confinement de 6 mois. Expliquez votre reaction','du','plus approbateur', 'au','plus contestataire');
INSERT INTO rankeddb.theme (id, user_id, main_text, determinant1, basse_intensite_text,determinant2, haute_intensite_text) VALUES (8, 1, 'Vous recevez votre vaccin contre la covid. Mimez votre reaction à l’injection','de','la plus nonchalante', 'à','la plus douloureuse');
INSERT INTO rankeddb.theme (id, user_id, main_text, determinant1, basse_intensite_text,determinant2, haute_intensite_text) VALUES (9, 1, 'Vous êtes en speed dating. Dites une phrase pour séduire votre interlocuteur', 'de','la pire', 'à','la meilleure');
INSERT INTO rankeddb.theme (id, user_id, main_text, determinant1, basse_intensite_text,determinant2, haute_intensite_text) VALUES (10, 1, 'Vos vacances de rêves tournent au cauchemar. Décrivez la raison', 'de','la plus sensée', 'à','la plus improbable');

-- Thèmes liés à un user pour simplifier la phase de test
INSERT INTO rankeddb.theme (id, user_id, main_text, determinant1, basse_intensite_text,determinant2, haute_intensite_text) VALUES (11, 4, 'Ce matin, au boulot, vous avez la tete dans le cul. A la machine a cafe, vous racontez votre nuit. ','de',' la nuit la plus pourrave', 'a',' la nuit de reve.');
INSERT INTO rankeddb.theme (id, user_id, main_text, determinant1, basse_intensite_text,determinant2, haute_intensite_text) VALUES (12, 4, 'En cherchant quelque chose dans la chambre de vos parents, vous trouvez un coffret et vous l ouvrez... Qu est-ce ? ', 'de',' pas genant', 'a',' tres genant.');

--
INSERT INTO rankeddb.deck (id, user_id, theme_id) VALUES (1,2,1);
INSERT INTO rankeddb.deck (id, user_id, theme_id) VALUES (2,2,2);
INSERT INTO rankeddb.deck (id, user_id, theme_id) VALUES (3,2,3);

INSERT INTO rankeddb.deck (id, user_id, theme_id) VALUES (4,3,1);
INSERT INTO rankeddb.deck (id, user_id, theme_id) VALUES (5,3,2);
INSERT INTO rankeddb.deck (id, user_id, theme_id) VALUES (6,3,3);

INSERT INTO rankeddb.deck (id, user_id, theme_id) VALUES (7,4,1);
INSERT INTO rankeddb.deck (id, user_id, theme_id) VALUES (8,4,2);
INSERT INTO rankeddb.deck (id, user_id, theme_id) VALUES (9,4,3);
INSERT INTO rankeddb.deck (id, user_id, theme_id) VALUES (10,4,4);
INSERT INTO rankeddb.deck (id, user_id, theme_id) VALUES (11,4,5);
INSERT INTO rankeddb.deck (id, user_id, theme_id) VALUES (12,4,6);
INSERT INTO rankeddb.deck (id, user_id, theme_id) VALUES (13,4,7);
INSERT INTO rankeddb.deck (id, user_id, theme_id) VALUES (14,4,8);
INSERT INTO rankeddb.deck (id, user_id, theme_id) VALUES (15,4,9);
INSERT INTO rankeddb.deck (id, user_id, theme_id) VALUES (16,4,10);


INSERT INTO rankeddb.deck (id, user_id, theme_id) VALUES (17,4,4);
INSERT INTO rankeddb.deck (id, user_id, theme_id) VALUES (18,4,5);


create table user
(
    id bigint auto_increment,
    constraint user_pk
        primary key (id),
    pseudo TEXT not null,
    email TEXT not null
);

create table theme
(
    id bigint auto_increment,
    constraint theme_pk
        primary key (id),
    user_id bigint not null references user (id),
    main_text TEXT not null,
    determinant1 TEXT not null,
    basse_intensite_text TEXT not null,
    determinant2 TEXT not null,
    haute_intensite_text TEXT not null
);

create table deck
(
    id bigint auto_increment,
    constraint deck_pk
        primary key (id),
    user_id bigint not null references user (id),
    theme_id bigint not null references theme (id)
);
